
package Menu;

public class terreno {
      //Atributos de la clase
    private int numLote;
    private float ancho;
    private float largo;
    
    public terreno(){
    this.numLote=0;
    this.ancho=0.0f;
    this.largo=0.0f;
    
    }
//Constructor por argumentos
    public terreno(int numLote, float ancho, float largo){
    this.numLote=numLote;
    this.ancho=ancho;
    this.largo=largo;
    }
    
    //Copia
    public terreno(terreno otro){
    this.numLote=otro.numLote;
    this.ancho=otro.ancho;
    this.largo=otro.largo;
    }
    
    //Metodos Set y Get

    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

    
    //Metodos de comportamiento
    public float calcularPerimetro(){
    float calcularPerimetro=0.0f;
    calcularPerimetro=2*(this.ancho+this.largo);
    return calcularPerimetro;
    }
    public float calcularArea(){
    float calcularArea=0.0f;
    calcularArea=this.ancho*this.largo;
    return calcularArea;
    }
    
}

