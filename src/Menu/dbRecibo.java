/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Menu;

import java.sql.*;

public class dbRecibo {
    private static final String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/alxsddd?user=alxsddd&password=uwu14252";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;

    // Constructor
    public dbRecibo() {
        try {
            Class.forName(MYSQLDRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("Surgió un error " + e.getMessage());
            System.exit(-1);
        }
    }

    public void conectar() {
        try {
            conexion = DriverManager.getConnection(MYSQLDB);
        } catch (SQLException e) {
            System.out.println("No se logró conectar " + e.getMessage());
        }
    }

    public void desconectar() {
        try {
            if (conexion != null && !conexion.isClosed()) {
                conexion.close();
            }
        } catch (SQLException e) {
            System.out.println("Surgió un error al desconectar " + e.getMessage());
        }
    }

    public void insertar(PagoRecibo rec) {
        conectar();
        try {
            strConsulta = "INSERT INTO recibo(numRecibo, fecha, nombre, domicilio, tipo, costo, consumo, status) VALUES (?, CURDATE(), ?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, rec.getNumRecibo());
            pst.setString(2, rec.getNom());
            pst.setString(3, rec.getDom());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCostKw());
            pst.setFloat(6, rec.getKwConsu(0));
            pst.setInt(7, rec.getStatus());

            pst.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error al insertar " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    public void actualizar(PagoRecibo rec) {
        strConsulta = "UPDATE recibo SET nombre=?, domicilio=?, fecha=CURDATE(), tipo=?, costo=?, consumo=? WHERE numRecibo=? and status=0";
        conectar();
        try {
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setString(1, rec.getNom());
            pst.setString(2, rec.getDom());
            pst.setInt(3, rec.getTipo());
            pst.setFloat(4, rec.getCostKw());
            pst.setFloat(5, rec.getKwConsu(0));
            pst.setInt(6, rec.getNumRecibo());

            pst.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Surgió un error al actualizar: " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    public void habilitar(PagoRecibo rec) {
        strConsulta = "UPDATE recibo SET status = 0 WHERE numRecibo = ?";
        conectar();
        try {
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, rec.getNumRecibo());

            pst.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Surgió un error al habilitar: " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    public void deshabilitar(PagoRecibo rec) {
        strConsulta = "UPDATE recibo SET status = 1 WHERE numrecibo = ?";
        conectar();
        try {
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, rec.getNumRecibo());

            pst.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Surgió un error al deshabilitar " + e.getMessage());
        } finally {
            desconectar();
        }
    }

    public boolean isExiste(int numRecibo, int status) {
        boolean exito = false;
        strConsulta = "SELECT * FROM recibo WHERE numrecibo = ? AND status = ?";
        try {
            conectar();
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, numRecibo);
            pst.setInt(2, status);
            this.registros = pst.executeQuery();
            if (this.registros.next()) {
                exito = true;
            }
        } catch (SQLException e) {
            System.err.println("Surgió un error al verificar si existe: " + e.getMessage());
        } finally {
            desconectar();
        }
        return exito;
    }

    public PagoRecibo buscar(int numRecibo) {
        PagoRecibo recibo = new PagoRecibo();
        conectar();
        try {
            strConsulta = "SELECT * FROM recibo WHERE numRecibo = ? AND status = 0";
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setInt(1, numRecibo);
            this.registros = pst.executeQuery();
            if (this.registros.next()) {
                recibo.setId(registros.getInt("id"));
                recibo.setNumRecibo(registros.getInt("numRecibo"));
                recibo.setNom(registros.getString("nombre"));
                recibo.setDom(registros.getString("domicilio"));
                recibo.setTipo(registros.getInt("tipo"));
                recibo.setCostKw(registros.getFloat("costo"));
                recibo.setKwConsu(registros.getFloat("consumo"));
                recibo.setFecha(registros.getString("fecha"));
            } else {
                recibo.setId(0);
            }
        } catch (SQLException e) {
            System.err.println("Surgió un error al buscar " + e.getMessage());
        } finally {
            desconectar();
        }
        return recibo;
    }
}
