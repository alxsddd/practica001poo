/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Menu;

/**
 *
 * @author Alexis
 */
public class TestdbRecibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Verificando errores");
        dbRecibo rec = new dbRecibo();
        rec.conectar();
        
        PagoRecibo recibo = new PagoRecibo();
        recibo.setNumRecibo(100);
        recibo.setNom("Jose Miguel ");
        recibo.setDom("Av Sol 33");
        recibo.setCostKw(10000f);
        recibo.setKwConsu(1211f);
        recibo.setTipo(1);
        recibo.setFecha("2023-12-08");
        
       rec.actualizar(recibo);
       
       rec.deshabilitar(recibo);
       if (rec.isExiste(100, 0)) System.out.println("Si existe");
       else System.out.println("No existe");
       rec.habilitar(recibo);
       if (rec.isExiste(100, 0)) System.out.println("Si existe");
       else System.out.println("No existe");
       
       PagoRecibo result= rec.buscar(100);
       
       if(result.getId()>0){
           System.out.println(result.getNom() + "dom" + result.getDom());
           
       }
    }
    
}
