/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;

/**
 *
 * @author Black
 */
 class PagoRecibo {
     private int id;
    private int numRecibo ;
    private String fecha;
    private String nom;
    private String dom;
    private int tipo;
    private float costKw;
    private float kwConsu;
    private int status;

    public PagoRecibo(){
        this.numRecibo = 0;
        this.fecha = "";
        this.nom = "";
        this.dom = "";
        this.id = 0;
        this.costKw = 0.00f;
        this.kwConsu = 0.0f;
        this.status=0;
    }

    public PagoRecibo(int id,int numRecibo, String fecha, String nom, int tipo, float costKw, float kwConsu, int status) {
        this.id = id;
        this.numRecibo = numRecibo;
        this.fecha = fecha;
        this.nom = nom;
        this.tipo = tipo;
        this.costKw = costKw;
        this.kwConsu = kwConsu;
        this.status = status;
    }
     
    public PagoRecibo(PagoRecibo otro) {
        this.id = id;
        this.numRecibo = otro.numRecibo;
        this.fecha = otro.fecha;
        this.nom = otro.nom;
        this.tipo = otro.tipo;
        this.costKw = otro.costKw;
        this.kwConsu = otro.kwConsu;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCostKw() {
        return costKw;
    }

    public void setCostKw(float costKw) {
        this.costKw = costKw;
    }

    public float getKwConsu(float kwConsu) {
        return kwConsu;
    }

    public void setKwConsu(float kwConsu) {
        this.kwConsu = kwConsu;
    }
    
    public float calcularSubtotal() {
        float subtotal = 0.0f;
        if (tipo == 1){this.costKw = 2.00f;
        subtotal = this.kwConsu * this.costKw;}
        if (tipo == 2){this.costKw = 3.00f;
        subtotal = this.kwConsu * this.costKw;}
        if (tipo == 3){this.costKw = 5.00f;
        subtotal = this.kwConsu * this.costKw;}
        
        return subtotal;                  
    }
    public float calcularImpuesto(){
    float impuesto = 0.0f;
    float sub = calcularSubtotal();
    impuesto = sub * 0.16f;
    return impuesto;
    }
    public float calcularTotalPagar(){
    float total = 0.0f;
    float sub = calcularSubtotal();
    float impuesto = calcularImpuesto();
    total = sub + impuesto;
    return total;
    }
    
    
}
