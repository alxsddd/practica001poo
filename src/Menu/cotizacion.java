/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menu;

/**
 *
 * @author Black
 */
public class cotizacion {
          //Atributos de la clase
    private int numCot;
    private String descAuto;
    private float precio;
    private float porPagoIn;
    private int plazo;
    
    public cotizacion(){
    this.numCot=0;
    this.descAuto="";
    this.precio=0.0f;
    this.porPagoIn=0.0f;
    this.plazo=0;
    
    }
//Constructor por argumentos
    public cotizacion(int numCot, String descAuto, float precio, float porPagoIn, int plazo){
    this.numCot=numCot;
    this.descAuto=descAuto;
    this.precio=precio;
    this.porPagoIn=porPagoIn;
    this.plazo=plazo;
    }
    
    //Copia
    public cotizacion(cotizacion otro){
    this.numCot=otro.numCot;
    this.descAuto=otro.descAuto;
    this.precio=otro.precio;
    this.porPagoIn=otro.porPagoIn;
    this.plazo=otro.plazo;
    }
    
    //Metodos Set y Get

    public int getNumCot() {
        return numCot;
    }

    public void setNumCot(int numCot) {
        this.numCot = numCot;
    }

    public String getDescAuto() {
        return descAuto;
    }

    public void setDescAuto(String descAuto) {
        this.descAuto = descAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorPagoIn() {
        return porPagoIn;
    }

    public void setPorPagoIn(float porPagoIn) {
        this.porPagoIn = porPagoIn;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }



    
    //Metodos de comportamiento
    public float calcularPagoIn(){
    float inicial=this.precio * (this.porPagoIn / 100 );
    return inicial;
    }
    public float calcularTotalFin(){
    float inicial = calcularPagoIn();
    float fin = this.precio - inicial;
    return fin;
}
    
    public float calcularPagoMen(){
   float mensual = 0;
   float fin = calcularTotalFin();
   mensual = fin / this.plazo;
   return mensual;
    }
    
}

